import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import Home from './pages/Home/Home'
import TodoList2 from './pages/TodoList/TodoList2'

import GlobalStyles from './styles'

import './config/ReactotronConfig'

import { store } from './store'

function App() {
  return (
    <Provider store={store}>
      <GlobalStyles />
      <Router>
        <Switch>
        <Route component={TodoList2} exact path="/todoList/:name?"/>
        <Route component={Home} exact path="/"/>
        </Switch>
      </Router>
    </Provider>
  ) 
}

export default App;
