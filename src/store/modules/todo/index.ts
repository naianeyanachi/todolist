import { Reducer } from 'redux'

import { TodoState, TodoTypes } from './types'

const INITIAL_STATE: TodoState = {
  name: '',
  todoList: []
}

const reducer: Reducer<TodoState> = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case TodoTypes.TODO_SUCCESS:
      return {...state, todoList: action.payload.todoList}
    default:
      return state
  }
}

export default reducer