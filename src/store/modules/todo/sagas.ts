import { call, all, takeLatest, put, select } from 'redux-saga/effects'

import api from '../../../services/api'

import { getTodoSuccess } from './actions'
import { AnyAction } from 'redux'

import { ApplicationState } from '../../index'

import { TodoTypes, TodoData } from './types'
import { uuid } from 'uuidv4'


function* addTodo({ payload }: AnyAction) {
    const todoList = yield select((state: ApplicationState) => state.todo.todoList)

    const newTodo = {
        message: payload.message,
        done: false
    }

    const callPost = yield call(api.post, '', newTodo)
    const callGet = yield call(api.get, '', {})
    yield put(getTodoSuccess(callGet.data))
}

function* deleleTodo({ payload }: AnyAction) {

    yield call(api.delete, '/' + payload.id)
    const callGet = yield call(api.get, '', {})

    yield put(getTodoSuccess(callGet.data))
}

function* changeTodo({ payload }: AnyAction) {
    const todoList = yield select((state: ApplicationState) => state.todo.todoList)

    let done

    todoList.forEach(function (todo: TodoData) {
        if (todo._id === payload.id) {
            done = !todo.done
        }
    })
       
    yield call(api.patch, '/' + payload.id, { done: done })
    const callGet = yield call(api.get, '', {})

    yield put(getTodoSuccess(callGet.data))
}


export default all([
  takeLatest(TodoTypes.ADD_TODO, addTodo),
  takeLatest(TodoTypes.DELETE_TODO, deleleTodo),
  takeLatest(TodoTypes.CHANGE_TODO, changeTodo)
])