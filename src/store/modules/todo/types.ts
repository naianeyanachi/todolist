export enum TodoTypes {
    'ADD_TODO' = '@todo/ADD_TODO',
    'DELETE_TODO' = '@todo/DELETE_TODO',
    'CHANGE_TODO' = '@todo/CHANGE_TODO',
    'TODO_SUCCESS' = '@todo/TODO_SUCCESS'
  }
  
  export interface TodoData {
    _id: string,
    message: string,
    done: boolean
  }
  
  export interface TodoState {
    name: string
    todoList: TodoData[]
  }