import { action } from 'typesafe-actions'
import { TodoTypes, TodoData } from './types'

export const addTodo = (message: string) => action(TodoTypes.ADD_TODO, { message })
export const deleteTodo = (id: string) => action(TodoTypes.DELETE_TODO, { id })
export const changeTodo = (id: string) => action(TodoTypes.CHANGE_TODO, { id })
export const getTodoSuccess = (todoList: TodoData[]) => action(TodoTypes.TODO_SUCCESS, { todoList })