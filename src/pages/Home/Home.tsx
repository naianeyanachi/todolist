import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'


function Home() {                          

    const [ todo, setTodo ] = useState('')
    const [ link, setLink ] = useState('')

    const history = useHistory()

    useEffect(() => {
        setLink("/todoList/" + todo)
    }, [todo]) 


    return (
        <div id='todoList'>
            <h1>Home</h1>

            <h3>Nome:</h3>
            <div className='todoInput'>
                <input 
                    type='text' 
                    value={todo} 
                    onChange={ e => setTodo(e.target.value)}
                />
                <button type='button' onClick={() => history.push(link)}>Go to Todo List</button>    
            </div>
        </div>
    )
}

export default Home
