import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import { ApplicationState } from '../../store/index'
import { call } from 'redux-saga/effects'

import api from '../../services/api'

import { deleteTodo, addTodo, changeTodo, getTodoSuccess } from '../../store/modules/todo/actions'

import TodoInput from '../../components/TodoInput'
import TodoList from '../../components/TodoList'

import { Container } from './styles'


function TodoList2 () {                          

    // const [ todoList1, setTodoList ] = useState<Todo[]>([])
    const [ todo, setTodo ] = useState('')

    let { name } = useParams()

    const dispatch = useDispatch()

    const todoList = useSelector((state: ApplicationState) => state.todo.todoList)

    useEffect(() => {
        const todoList = localStorage.getItem('todoList')

        if (todoList) {
            dispatch(getTodoSuccess(JSON.parse(todoList)))
        }
    }, [])

    useEffect(() => {
        localStorage.setItem('todoList', JSON.stringify(todoList))
    }, [todoList])

    function handleDelete(id: string) {
        dispatch(deleteTodo(id))
    }

    function handleChange(id: string) {
        dispatch(changeTodo(id))
    }

    function handleAdd() {
        dispatch(addTodo(todo))
    }


    return (
        <Container>
            <h4>Hello, <span>{name ?? 'User' }</span></h4>
            <h1>TodoList</h1>

            <TodoInput 
                value={todo} 
                onChange={setTodo} 
                onClick={handleAdd} />

            <TodoList 
                todoList={todoList}
                handleDelete={handleDelete}
                handleChange={handleChange} />
        </Container>
    )
}

export default TodoList2