import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
    }

    * {
        box-sizing: border-box;
        outline: none;
    }

    body, button {
        color: #717171;
        font-family: Helvetica, sans-serif;

    button.icon {
        border: none;
        background: none;
    }

    button.icon:active {
        transform: scale(1.1);
    }

}
`


export default GlobalStyles