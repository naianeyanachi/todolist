import React from 'react'

import Todo from '../Todo'
import { TodoData } from '../../store/modules/todo/types'

interface Props {
  todoList: TodoData[]
  handleChange: (id: string) => void
  handleDelete: (id: string) => void
}

const TodoList: React.FC<Props> = ({ todoList, handleChange, handleDelete }: Props) => {

  return (
    <>
        {
            todoList.length === 0 ? 
                (<h3>Lista Vazia</h3>)
            :
                todoList.map(todo => 
                    <Todo 
                        todo={todo} 
                        handleDelete={handleDelete} 
                        handleChange={handleChange} />)
        }
    </>
  )
}

export default TodoList