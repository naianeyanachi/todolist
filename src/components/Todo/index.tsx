import React from 'react'

import { AiFillCheckCircle, AiFillDelete } from 'react-icons/ai'
import { BsArrowCounterclockwise } from 'react-icons/bs'

import { TodoItem, TodoButton } from './styles'

export interface TodoData {
    _id: string,
    message: string,
    done: boolean
  }

interface Todo {
  todo: TodoData
  handleDelete: (id: string) => void
  handleChange: (id: string) => void
}

const Todo: React.FC<Todo> = ({ todo, handleDelete, handleChange }: Todo) => {

  return (
    <TodoItem id={todo._id}>
        <div>{todo.message}</div>
        <div className='actions'>
            <TodoButton type='button' onClick={() => handleChange(todo._id)} className='icon'>
            {
                !todo.done ?
                    <AiFillCheckCircle color='#27ae60' /> :
                    <BsArrowCounterclockwise color='#e67e22' />
            }
            </TodoButton>
            <TodoButton type='button' onClick={() => handleDelete(todo._id)} className='icon'>
                <AiFillDelete color='#e74c3c' />
            </TodoButton>
        </div>
    </TodoItem>
  )
}

export default Todo