import styled from 'styled-components'

export const TodoItem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
  margin: 10px 0;
  border: 1px solid #eaeaea;
  border-radius: 10px;
`

export const TodoButton = styled.button`
  cursor: pointer;

  svg {
    font-size: 20px;
  }
`