import React from 'react'

import { TodoInputContainer } from './styles'

interface Props {
  value: string
  onChange: (todoContent: string) => void
  onClick: () => void
}

const TodoInput: React.FC<Props> = ({ value, onChange, onClick }: Props) => {

  return (
    <TodoInputContainer>
        <input 
            type='text' 
            value={value} 
            onChange={ e => onChange(e.target.value)}
        />
        <button 
            type='button' 
            className='addTodo'
            onClick={onClick}
        >Add</button>    
    </TodoInputContainer>
  )
}

export default TodoInput