import styled from 'styled-components'

export const TodoInputContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 40px;
    
    input {
        height: 30px;
        border-radius: 5px;
        border: 1px solid #ccc
    }

    button {
        height: 30px;
        width: 60px;
        border: 1px solid #eaeaea;
        border-radius: 5px;
        margin-left: 5px;
    }
`
